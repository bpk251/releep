import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:releep_app/utils/constants.dart';

class MenuTime extends StatefulWidget {
  @override
  _MenuTime createState() => _MenuTime();
}

class _MenuTime extends State<MenuTime> {
  TextEditingController _controller4;
  String _valueChanged4 = '';
  String _valueToValidate4 = '';
  String _valueSaved4 = '';

  bool _enableMon = false;
  bool _enableTue = false;
  bool _enableWed = false;
  bool _enableThu = false;
  bool _enableFri = false;
  bool _enableSat = false;
  bool _enableSun = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    String lsHour = TimeOfDay.now().hour.toString().padLeft(2, '0');
    String lsMinute = TimeOfDay.now().minute.toString().padLeft(2, '0');
    _controller4 = TextEditingController(text: '$lsHour:$lsMinute');
  }

  Future<void> _getValue() async {
    await Future.delayed(const Duration(seconds: 3), () {
      setState(() {
        _controller4.text = '17:01';
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Time Seeing'),
        backgroundColor: HexColor("#633ce0"),
      ),
      body: Padding(
        padding: EdgeInsets.all(paddingContainer),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Image.asset(
              'assets/images/releep.png',
              height: 200,
            ),
            SizedBox(
              height: 30,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Change Time',
                      style: TextStyle(
                          color: HexColor('#364bc4'),
                          fontSize: 13,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Divider(
                      height: 1,
                      color: Colors.black87,
                    ),
                    DateTimePicker(
                      type: DateTimePickerType.time,
                      controller: _controller4,
                      //initialValue: _initialValue,
                      icon: Icon(Icons.access_time),
                      // timeLabelText: "Time",
                      use24HourFormat: false,
                      locale: Locale('th', 'TH'),
                      onChanged: (val) => setState(() => _valueChanged4 = val),
                      validator: (val) {
                        setState(() => _valueToValidate4 = val);
                        return null;
                      },
                      onSaved: (val) => setState(() => _valueSaved4 = val),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Text(
                      'Repeat',
                      style: TextStyle(
                          color: HexColor('#364bc4'),
                          fontSize: 13,
                          fontWeight: FontWeight.bold),
                    ),
                    SwitchListTile(
                      title: Text(
                        'Every Sunday',
                        style: TextStyle(fontSize: 12),
                      ),
                      value: _enableSun,
                      onChanged: (bool value) {
                        setState(
                          () {
                            _enableSun = value;
                          },
                        );
                      },
                    ),
                    SwitchListTile(
                      title: Text(
                        'Every Monday',
                        style: TextStyle(fontSize: 12),
                      ),
                      value: _enableMon,
                      onChanged: (bool value) {
                        setState(
                          () {
                            _enableMon = value;
                          },
                        );
                      },
                    ),
                    SwitchListTile(
                      title: Text(
                        'Every Tuesday',
                        style: TextStyle(fontSize: 12),
                      ),
                      value: _enableTue,
                      onChanged: (bool value) {
                        setState(
                          () {
                            _enableTue = value;
                          },
                        );
                      },
                    ),
                    SwitchListTile(
                      title: Text(
                        'Every Wednesday',
                        style: TextStyle(fontSize: 12),
                      ),
                      value: _enableWed,
                      onChanged: (bool value) {
                        setState(
                          () {
                            _enableWed = value;
                          },
                        );
                      },
                    ),
                    SwitchListTile(
                      title: Text(
                        'Every Thursday',
                        style: TextStyle(fontSize: 12),
                      ),
                      value: _enableThu,
                      onChanged: (bool value) {
                        setState(
                          () {
                            _enableThu = value;
                          },
                        );
                      },
                    ),
                    SwitchListTile(
                      title: Text(
                        'Every Friday',
                        style: TextStyle(fontSize: 12),
                      ),
                      value: _enableFri,
                      onChanged: (bool value) {
                        setState(
                          () {
                            _enableFri = value;
                          },
                        );
                      },
                    ),
                    SwitchListTile(
                      title: Text(
                        'Every Saturday',
                        style: TextStyle(fontSize: 12),
                      ),
                      value: _enableSat,
                      onChanged: (bool value) {
                        setState(
                          () {
                            _enableSat = value;
                          },
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget _buildPopupDialog(BuildContext context) {
  return new AlertDialog(
    title: const Text('Popup example'),
    content: new Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("Hello"),
      ],
    ),
    actions: <Widget>[
      new FlatButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        textColor: Theme.of(context).primaryColor,
        child: const Text('Close'),
      ),
    ],
  );
}
