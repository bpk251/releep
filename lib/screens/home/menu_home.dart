import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:provider/provider.dart';
import 'package:releep_app/providers/alarm_provider.dart';
import 'package:toast/toast.dart';
import '../../utils/constants.dart';
import 'add_alarm.dart';

class MenuHome extends StatefulWidget {
  @override
  _MenuHome createState() => _MenuHome();
}

class _MenuHome extends State<MenuHome> {
  bool status = true;
  AlarmProvider alarmProvider;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var provider = Provider.of<AlarmProvider>(context, listen: false);
    provider.getAlarm();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Home'),
        backgroundColor: HexColor("#633ce0"),
      ),
      body: Padding(
        padding: EdgeInsets.all(paddingContainer),
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Image.asset(
              'assets/images/releep.png',
              height: 200,
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              children: [
                Expanded(
                  child: RaisedButton(
                    onPressed: () {
                      alarmProvider.resetAlarmAll();
                    },
                    color: Colors.amber,
                    child: Text(
                      'reset all/stop all alarm'.toUpperCase(),
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    ),
                  ),
                )
              ],
            ),
            Consumer(
              builder: (context, AlarmProvider provider, Widget child) {
                alarmProvider = provider;
                var count = provider.alarms.length;
                if (provider.isLoading) {
                  return Expanded(
                    flex: 1,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }
                if (provider.isEmty) {
                  return Expanded(
                    flex: 1,
                    child: Center(
                      child: Text(
                        'Emty',
                        style: TextStyle(color: Colors.black12),
                      ),
                    ),
                  );
                }
                return Expanded(
                  flex: 1,
                  child: ListView.builder(
                    itemCount: count,
                    itemBuilder: (context, index) {
                      var alarmData = provider.alarms[index];
                      return Column(
                        children: [
                          Container(
                            height: 185,
                            width: double.infinity,
                            child: Card(
                              color: HexColor("#f0efff"),
                              child: Padding(
                                padding: EdgeInsets.all(15.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: 30,
                                      width: 70,
                                      child: RaisedButton(
                                        onPressed: () {
                                          provider.deleteAlarm(index);
                                        },
                                        color: Colors.red,
                                        child: Text(
                                          'DELETE',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 10),
                                        ),
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          width: 200,
                                          child: Text(
                                            'Weke up ${alarmData.dateString} ${alarmData.time}',
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.blue,
                                            ),
                                          ),
                                        ),
                                        Switch(
                                          value: alarmData.enableAlarm,
                                          onChanged: (value) {
                                            provider.updateEnableAlarm(
                                                index, value);
                                          },
                                          activeTrackColor: Colors.blue[200],
                                          activeColor: Colors.blue,
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(5.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          InkWell(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            onTap: () {},
                                            child: Padding(
                                              padding: EdgeInsets.all(10.0),
                                              child: Column(
                                                children: [
                                                  Image.asset(
                                                    'assets/images/smell.png',
                                                    height: 20,
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Container(
                                                    width: 50,
                                                    child: Text(
                                                      alarmData.smell,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 9,
                                                      ),
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          InkWell(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            onTap: () {},
                                            child: Padding(
                                              padding: EdgeInsets.all(10.0),
                                              child: Column(
                                                children: [
                                                  Image.asset(
                                                    'assets/images/sound.png',
                                                    height: 20,
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Container(
                                                    width: 50,
                                                    child: Text(
                                                      alarmData.sound,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 9,
                                                      ),
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          InkWell(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            onTap: () {},
                                            child: Padding(
                                              padding: EdgeInsets.all(10.0),
                                              child: Column(
                                                children: [
                                                  Image.asset(
                                                    'assets/images/shake.png',
                                                    height: 20,
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Container(
                                                    width: 50,
                                                    child: Text(
                                                      'Shake ${alarmData.valueShake.toInt()}%',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 9,
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          InkWell(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            onTap: () {
                                              // Toast.show('Roller - ${alarmData.enableRoller}', context,
                                              //     duration: 2,
                                              //     gravity: Toast.CENTER);
                                            },
                                            child: Padding(
                                              padding: EdgeInsets.all(10.0),
                                              child: Column(
                                                children: [
                                                  Image.asset(
                                                    'assets/images/roller_true.png',
                                                    height: 20,
                                                  ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Container(
                                                    width: 50,
                                                    child: Text(
                                                      'Roller - ${alarmData.enableRoller}',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 9,
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      );
                    },
                  ),
                );
              },
            )
          ],
        ),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: [
              SizedBox(
                height: 72,
              ),
              FloatingActionButton(
                elevation: 8,
                heroTag: 'save',
                onPressed: () {
                  // What you want to do
                },
                child: Image.asset(
                  'assets/images/question.png',
                  height: 20,
                ),
                backgroundColor: HexColor("#24d6ea"),
              ),
            ],
          ),
          FloatingActionButton(
            heroTag: 'add',
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AddAlarm(),
                ),
              );
            },
            backgroundColor: HexColor("#633ce0"),
            child: Icon(Icons.add),
          ),
        ],
      ),
    );
  }
}
