import 'package:chips_choice/chips_choice.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:provider/provider.dart';
import 'package:releep_app/models/alarm.dart';
import 'package:releep_app/providers/alarm_provider.dart';
import '../../utils/constants.dart';

class AddAlarm extends StatefulWidget {
  @override
  _AddAlarmState createState() => _AddAlarmState();
}

class _AddAlarmState extends State<AddAlarm> {
  TextEditingController __timeController;
  String _valueChanged = '';
  String _valueToValidate = '';
  String _valueSaved = '';

  bool _selectSmell1 = true;
  bool _selectSmell2 = false;
  bool _selectSmell3 = false;
  String _colorSmellCard1 = "#1ce370";
  String _colorSmellCard2 = "#dde9ff";
  String _colorSmellCard3 = "#dde9ff";

  bool _selectSound1 = true;
  bool _selectSound2 = false;
  bool _selectSound3 = false;
  bool _selectSound4 = false;
  String _colorSoundCard1 = "#1ce370";
  String _colorSoundCard2 = "#dde9ff";
  String _colorSoundCard3 = "#dde9ff";
  String _colorSoundCard4 = "#dde9ff";

  var _enableShakeSetting = false;
  var _enableSheetRoller = false;
  double _currentSliderValue = 10;

  List<Map<String, String>> days = [
    {'value': 'mon', 'title': 'Monday'},
    {'value': 'tue', 'title': 'Tuesday'},
    {'value': 'wed', 'title': 'Wednesday'},
    {'value': 'thu', 'title': 'Thursday'},
    {'value': 'fri', 'title': 'Friday'},
    {'value': 'sat', 'title': 'Saturday'},
    {'value': 'sun', 'title': 'Sunday'},
  ];

  List<String> tags = [
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat',
    'Sun',
  ];
  List<String> options = [
    'All',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat',
    'Sun',
  ];

  var _dateSelected;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    String lsHour = TimeOfDay.now().hour.toString().padLeft(2, '0');
    String lsMinute = TimeOfDay.now().minute.toString().padLeft(2, '0');
    __timeController = TextEditingController(text: '$lsHour:$lsMinute');
  }

  Future<void> _getValue() async {
    await Future.delayed(const Duration(seconds: 3), () {
      setState(() {
        __timeController.text = '17:01';
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor("#633ce0"),
      ),
      body: Consumer(
        builder: (context, AlarmProvider provider, Widget child) {
          return Padding(
            padding: EdgeInsets.all(paddingContainer),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Change Alarm Time',
                        style: TextStyle(
                          color: Colors.black87,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Divider(),
                      _DateTimePicker(),
                      Divider(),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Select Date',
                        style: TextStyle(
                          color: Colors.black87,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      ChipsChoice<String>.multiple(
                        value: tags,
                        onChanged: (val) {
                          if (val.contains("All")) {
                            if (tags.isEmpty) {
                              setState(() {
                                tags = [
                                  'Mon',
                                  'Tue',
                                  'Wed',
                                  'Thu',
                                  'Fri',
                                  'Sat',
                                  'Sun',
                                ];
                              });
                            } else {
                              setState(() {
                                tags.clear();
                              });
                            }
                          } else {
                            setState(() {
                              tags = val;
                            });
                          }
                        },
                        choiceItems: C2Choice.listFrom<String, String>(
                          source: options,
                          value: (i, v) => v,
                          label: (i, v) => v,
                        ),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      Center(
                        child: Text(
                          'Select Smell',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                            color: HexColor("#364bc4"),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      _SmellWidgets(),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 50,
                      ),
                      Center(
                        child: Text(
                          'Select Sound',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: HexColor("#364bc4"),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      _SoundWidgets(),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _ShakSettingWidget(),
                  SizedBox(
                    height: 20,
                  ),
                  _SheetRollerWidget(),
                  Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: RaisedButton(
                          color: HexColor("#7044ff"),
                          onPressed: () {
                            String _smell;
                            String _sound;
                            if (_selectSmell1) {
                              _smell = "Lavender";
                            } else if (_selectSmell2) {
                              _smell = "Coffee";
                            } else if (_selectSmell3) {
                              _smell = "Flower";
                            }

                            if (_selectSound1) {
                              _sound = "Classic";
                            } else if (_selectSound2) {
                              _sound = "Cat";
                            } else if (_selectSound3) {
                              _sound = "Natural";
                            } else if (_selectSound4) {
                              _sound = "Other";
                            }

                            var _dateString = "";
                            if (tags.length == 7) {
                              _dateString = "daily";
                            } else {
                              tags.forEach((element) {
                                _dateString = _dateString + "$element, ";
                              });
                            }

                            Alarm alarm = Alarm(
                                id: DateTime.now()
                                    .microsecondsSinceEpoch
                                    .toString(),
                                time: __timeController.text,
                                date: tags,
                                dateString: _dateString,
                                enableShake: _enableShakeSetting,
                                enableRoller: _enableSheetRoller,
                                sound: _sound,
                                smell: _smell,
                                enableAlarm: true,
                                valueShake: _currentSliderValue);

                            var provider = Provider.of<AlarmProvider>(context,
                                listen: false);
                            provider.addAlarm(alarm);
                            Navigator.pop(context);
                          },
                          child: Text(
                            'SAVE ALARM TIME',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _DateTimePicker() {
    return DateTimePicker(
      type: DateTimePickerType.time,
      controller: __timeController,
      //initialValue: _initialValue,
      icon: Icon(Icons.access_time),
      // timeLabelText: "Time",
      use24HourFormat: false,
      locale: Locale('th', 'TH'),
      onChanged: (val) => setState(() => _valueChanged = val),
      validator: (val) {
        setState(() => _valueToValidate = val);
        return null;
      },
      onSaved: (val) => setState(() => _valueSaved = val),
    );
  }

  Widget _SmellWidgets() {
    return Center(
      child: Wrap(
        children: [
          InkWell(
            borderRadius: BorderRadius.circular(8.0),
            onTap: () {
              setState(() {
                _selectSmell1 = true;
                _selectSmell2 = false;
                _selectSmell3 = false;
                _colorSmellCard1 = "#1ce370";
                _colorSmellCard2 = "#dde9ff";
                _colorSmellCard3 = "#dde9ff";
              });
            },
            child: Container(
              width: 165,
              height: 120,
              child: Card(
                child: Padding(
                  padding: EdgeInsets.all(15.0),
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/images/smell.png',
                        height: 50,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        width: 80,
                        alignment: Alignment.center,
                        child: Text(
                          'Lavander'.toUpperCase(),
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
                color: HexColor(_colorSmellCard1),
              ),
            ),
          ),
          InkWell(
            borderRadius: BorderRadius.circular(8.0),
            onTap: () {
              setState(() {
                _selectSmell1 = false;
                _selectSmell2 = true;
                _selectSmell3 = false;
                _colorSmellCard1 = "#dde9ff";
                _colorSmellCard2 = _selectSmell2 ? "#1ce370" : "#dde9ff";
                _colorSmellCard3 = "#dde9ff";
              });
            },
            child: Container(
              width: 165,
              height: 120,
              child: Card(
                child: Padding(
                  padding: EdgeInsets.all(15.0),
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/images/smell.png',
                        height: 50,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        width: 80,
                        alignment: Alignment.center,
                        child: Text(
                          'coffee'.toUpperCase(),
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
                color: HexColor(_colorSmellCard2),
              ),
            ),
          ),
          InkWell(
            borderRadius: BorderRadius.circular(8.0),
            onTap: () {
              setState(() {
                _selectSmell1 = false;
                _selectSmell2 = false;
                _selectSmell3 = true;
                _colorSmellCard1 = "#dde9ff";
                _colorSmellCard2 = "#dde9ff";
                _colorSmellCard3 = _selectSmell3 ? "#1ce370" : "#dde9ff";
              });
            },
            child: Container(
              width: 165,
              height: 120,
              child: Card(
                child: Padding(
                  padding: EdgeInsets.all(15.0),
                  child: Column(
                    children: [
                      Image.asset(
                        'assets/images/smell.png',
                        height: 50,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Container(
                        width: 80,
                        alignment: Alignment.center,
                        child: Text(
                          'flower'.toUpperCase(),
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 15,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
                color: HexColor(_colorSmellCard3),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _SoundWidgets() {
    return Center(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                borderRadius: BorderRadius.circular(8.0),
                onTap: () {
                  setState(() {
                    _selectSound1 = true;
                    _selectSound2 = false;
                    _selectSound3 = false;
                    _selectSound4 = false;
                    _colorSoundCard1 = "1ce370";
                    _colorSoundCard2 = "#dde9ff";
                    _colorSoundCard3 = "#dde9ff";
                    _colorSoundCard4 = "#dde9ff";
                  });
                },
                child: Container(
                  width: 110,
                  height: 110,
                  child: Card(
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/images/sound_setting.png',
                            height: 25,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: Text(
                              'Classic Alarms',
                              style: TextStyle(
                                color: Colors.black54,
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    color: HexColor(_colorSoundCard1),
                  ),
                ),
              ),
              InkWell(
                borderRadius: BorderRadius.circular(8.0),
                onTap: () {
                  setState(() {
                    _selectSound1 = false;
                    _selectSound2 = true;
                    _selectSound3 = false;
                    _selectSound4 = false;
                    _colorSoundCard1 = "#dde9ff";
                    _colorSoundCard2 = "#1ce370";
                    _colorSoundCard3 = "#dde9ff";
                    _colorSoundCard4 = "#dde9ff";
                  });
                },
                child: Container(
                  width: 110,
                  height: 110,
                  child: Card(
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/images/sound_setting.png',
                            height: 25,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: Text(
                              'Cat Sound',
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    color: HexColor(_colorSoundCard2),
                  ),
                ),
              ),
              InkWell(
                borderRadius: BorderRadius.circular(8.0),
                onTap: () {
                  setState(() {
                    _selectSound1 = false;
                    _selectSound3 = false;
                    _selectSound3 = true;
                    _selectSound4 = false;
                    _colorSoundCard1 = "#dde9ff";
                    _colorSoundCard2 = "#dde9ff";
                    _colorSoundCard3 = "#1ce370";
                    _colorSoundCard4 = "#dde9ff";
                  });
                },
                child: Container(
                  width: 110,
                  height: 110,
                  child: Card(
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/images/sound_setting.png',
                            height: 25,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: Text(
                              'Natural Sound',
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    color: HexColor(_colorSoundCard3),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InkWell(
                borderRadius: BorderRadius.circular(8.0),
                onTap: () {
                  setState(() {
                    _selectSound1 = false;
                    _selectSound2 = false;
                    _selectSound3 = false;
                    _selectSound4 = true;
                    _colorSoundCard1 = "#dde9ff";
                    _colorSoundCard2 = "#dde9ff";
                    _colorSoundCard3 = "#dde9ff";
                    _colorSoundCard4 = "#1ce370";
                  });
                },
                child: Container(
                  width: 140,
                  height: 120,
                  child: Card(
                    child: Padding(
                      padding: EdgeInsets.all(15.0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/images/sound_setting.png',
                            height: 35,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: Text(
                              'Other Sound',
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                    color: HexColor(_colorSoundCard4),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 1,
                child: Container(
                  height: 70,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(8.0),
                    onTap: () {},
                    child: Card(
                      child: Padding(
                        padding: EdgeInsets.all(15.0),
                        child: RaisedButton.icon(
                          onPressed: null,
                          icon: Icon(
                            Icons.mic_none,
                            color: Colors.white,
                          ),
                          label: Text(
                            'record voice'.toUpperCase(),
                            style: TextStyle(fontSize: 9, color: Colors.white),
                          ),
                          disabledColor: Colors.red,
                        ),
                      ),
                      color: HexColor("#dde9ff"),
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _ShakSettingWidget() {
    return Card(
      color: HexColor("#dde9ff"),
      child: Container(
        width: double.infinity,
        height: 400,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Text(
                'Shake Setting',
                style: TextStyle(
                    color: HexColor("#364bc4"),
                    fontSize: 14,
                    fontWeight: FontWeight.bold),
              ),
              Image.asset(
                'assets/images/bed.png',
                width: 250,
              ),
              Row(
                children: [
                  Container(
                    alignment: Alignment.centerRight,
                    child: Text(
                      "0",
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Slider(
                      value: _currentSliderValue,
                      min: 0,
                      max: 100,
                      divisions: 100,
                      activeColor: HexColor("#7044ff"),
                      inactiveColor: HexColor("#ddd3ff"),
                      label: _currentSliderValue.round().toString(),
                      onChanged: (double value) {
                        setState(() {
                          _currentSliderValue = value;
                        });
                      },
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerRight,
                    child: Text(
                      "100",
                    ),
                  ),
                ],
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                clipBehavior: Clip.hardEdge,
                child: Container(
                  width: double.infinity,
                  color: Colors.white,
                  child: SwitchListTile(
                    title: Text(
                      'switch ON/OFF',
                      style: TextStyle(fontSize: 12),
                    ),
                    value: _enableShakeSetting,
                    onChanged: (bool value) {
                      setState(
                        () {
                          _enableShakeSetting = value;
                        },
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _SheetRollerWidget() {
    return Card(
      color: HexColor("#dde9ff"),
      child: Container(
        width: double.infinity,
        height: 360,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              Text(
                'Sheet Roller',
                style: TextStyle(
                    color: HexColor("#364bc4"),
                    fontSize: 14,
                    fontWeight: FontWeight.bold),
              ),
              Image.asset(
                'assets/images/sheet_roller.gif',
                width: 250,
              ),
              SizedBox(
                height: 10,
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                clipBehavior: Clip.hardEdge,
                child: Container(
                  width: double.infinity,
                  color: Colors.white,
                  child: SwitchListTile(
                    title: Text(
                      'switch ON/OFF',
                      style: TextStyle(fontSize: 12),
                    ),
                    value: _enableSheetRoller,
                    onChanged: (bool value) {
                      setState(
                        () {
                          _enableSheetRoller = value;
                        },
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
