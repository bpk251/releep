import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:releep_app/utils/constants.dart';

class MenuBand extends StatefulWidget {
  @override
  _MenuBand createState() => _MenuBand();
}

class _MenuBand extends State<MenuBand> {
  var status = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Band Setting'),
        backgroundColor: HexColor("#633ce0"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(paddingContainer),
        child: Column(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Card(
                    elevation: 5,
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          width: 110,
                          child: Image.asset(
                            'assets/images/releep.png',
                            width: 80,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Expanded(
                            flex: 1,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: Container(
                                width: 200,
                                height: 120,
                                color: HexColor("#f0f0f0"),
                                child: Padding(
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                    children: [
                                      Stack(
                                        children: [
                                          Expanded(
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.end,
                                              children: [
                                                Icon(
                                                  Icons.settings,
                                                  color: Colors.black45,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Center(
                                            child: Column(
                                              children: [
                                                Text(
                                                  "Fall Detection",
                                                  style: TextStyle(
                                                      color:
                                                      HexColor("#364bc4"),
                                                      fontWeight:
                                                      FontWeight.bold),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Switch(
                                                  value: status,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      status = value;
                                                    });
                                                  },
                                                  activeTrackColor:
                                                  Colors.blue,
                                                  activeColor:
                                                  Colors.blue[400],
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'Fall Detection Status : ',
                                                      style: TextStyle(
                                                          fontSize: 12),
                                                    ),
                                                    Text(
                                                      'Enable',
                                                      style: TextStyle(
                                                          color: Colors
                                                              .lightGreen,
                                                          fontWeight:
                                                          FontWeight
                                                              .bold),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    elevation: 5,
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          width: 110,
                          child: Image.asset(
                            'assets/images/heart.png',
                            width: 80,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Expanded(
                            flex: 1,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: Container(
                                width: 200,
                                height: 120,
                                color: HexColor("#f0f0f0"),
                                child: Padding(
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                    children: [
                                      Stack(
                                        children: [
                                          Expanded(
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.end,
                                              children: [
                                                Icon(
                                                  Icons.settings,
                                                  color: Colors.black45,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Center(
                                            child: Column(
                                              children: [
                                                Text(
                                                  "Heart Rate"
                                                      "",
                                                  style: TextStyle(
                                                      color:
                                                      HexColor("#364bc4"),
                                                      fontWeight:
                                                      FontWeight.bold),
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Row(
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      '052 ',
                                                      style: TextStyle(
                                                          fontSize: 17,
                                                          color: Colors.red,
                                                          fontWeight:
                                                          FontWeight
                                                              .bold),
                                                    ),
                                                    Text(
                                                      'Enable',
                                                      style: TextStyle(
                                                          fontSize: 9,
                                                          color:
                                                          Colors.black12),
                                                    ),
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .center,
                                                  children: [
                                                    Text(
                                                      'Fall Detection Status : ',
                                                      style: TextStyle(
                                                          fontSize: 12),
                                                    ),
                                                    Text(
                                                      'Enable',
                                                      style: TextStyle(
                                                          color: Colors
                                                              .lightGreen,
                                                          fontWeight:
                                                          FontWeight
                                                              .bold),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    elevation: 5,
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          width: 110,
                          child: Image.asset(
                            'assets/images/sleep.png',
                            width: 80,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Expanded(
                            flex: 1,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: Container(
                                width: 200,
                                height: 120,
                                color: HexColor("#f0f0f0"),
                                child: Padding(
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                    children: [
                                      Stack(
                                        children: [
                                          Expanded(
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.end,
                                              children: [
                                                Icon(
                                                  Icons.settings,
                                                  color: Colors.black45,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Center(
                                            child: Column(
                                              children: [
                                                Text(
                                                  "Sleep Hour",
                                                  style: TextStyle(
                                                      color:
                                                      HexColor("#364bc4"),
                                                      fontWeight:
                                                      FontWeight.bold),
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Text(
                                                  '6h:34m',
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight:
                                                      FontWeight.bold,
                                                      fontSize: 19),
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Text(
                                                  'Friday, 29 May 2015',
                                                  style:
                                                  TextStyle(fontSize: 12),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Card(
                    elevation: 5,
                    child: Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          width: 110,
                          child: Image.asset(
                            'assets/images/sleep_walk.png',
                            width: 80,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Expanded(
                            flex: 1,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: Container(
                                width: 200,
                                height: 120,
                                color: HexColor("#f0f0f0"),
                                child: Padding(
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                    children: [
                                      Stack(
                                        children: [
                                          Expanded(
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.end,
                                              children: [
                                                Icon(
                                                  Icons.settings,
                                                  color: Colors.black45,
                                                ),
                                              ],
                                            ),
                                          ),
                                          Center(
                                            child: Column(
                                              children: [
                                                Text(
                                                  "Sleep Walk",
                                                  style: TextStyle(
                                                      color:
                                                      HexColor("#364bc4"),
                                                      fontWeight:
                                                      FontWeight.bold),
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Text(
                                                  '6h:34m',
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight:
                                                      FontWeight.bold,
                                                      fontSize: 19),
                                                ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                Text(
                                                  'Friday, 29 May 2015',
                                                  style:
                                                  TextStyle(fontSize: 12),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
