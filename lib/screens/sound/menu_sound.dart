import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:releep_app/utils/constants.dart';

class MenuSound extends StatefulWidget {
  @override
  _MenuSound createState() => _MenuSound();
}

class _MenuSound extends State<MenuSound> {
  bool onTapCard1 = false;
  String colorCard1 = "#dde9ff";
  bool onTapCard2 = false;
  String colorCard2 = "#dde9ff";
  bool onTapCard3 = false;
  String colorCard3 = "#dde9ff";
  bool onTapCard4 = false;
  String colorCard4 = "#dde9ff";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Sound'),
          backgroundColor: HexColor("#633ce0"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(paddingContainer),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(
                height: 20,
              ),
              Image.asset(
                'assets/images/releep.png',
                height: 200,
              ),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      InkWell(
                        borderRadius: BorderRadius.circular(8.0),
                        onTap: () {
                          setState(() {
                            onTapCard1 = !onTapCard1;
                            colorCard1 = onTapCard1 ? "#1ce370" : "#dde9ff";
                          });
                        },
                        child: Container(
                          width: 100,
                          height: 110,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.all(15.0),
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/images/sound_setting.png',
                                    height: 25,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      'Classic Alarms',
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            color: HexColor(colorCard1),
                          ),
                        ),
                      ),
                      InkWell(
                        borderRadius: BorderRadius.circular(8.0),
                        onTap: () {
                          setState(() {
                            onTapCard2 = !onTapCard2;
                            colorCard2 = onTapCard2 ? "#1ce370" : "#dde9ff";
                          });
                        },
                        child: Container(
                          width: 100,
                          height: 110,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.all(15.0),
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/images/sound_setting.png',
                                    height: 25,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      'Cat Sound',
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            color: HexColor(colorCard2),
                          ),
                        ),
                      ),
                      InkWell(
                        borderRadius: BorderRadius.circular(8.0),
                        onTap: () {
                          setState(() {
                            onTapCard3 = !onTapCard3;
                            colorCard3 = onTapCard3 ? "#1ce370" : "#dde9ff";
                          });
                        },
                        child: Container(
                          width: 100,
                          height: 110,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.all(15.0),
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/images/sound_setting.png',
                                    height: 25,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      'Natural Sound',
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            color: HexColor(colorCard3),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        borderRadius: BorderRadius.circular(8.0),
                        onTap: () {},
                        child: Container(
                          width: 140,
                          height: 120,
                          child: Card(
                            child: Padding(
                              padding: EdgeInsets.all(15.0),
                              child: Column(
                                children: [
                                  Image.asset(
                                    'assets/images/sound_setting.png',
                                    height: 35,
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      'Other Sound',
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            color: HexColor("#dde9ff"),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          height: 70,
                          child: InkWell(
                            borderRadius: BorderRadius.circular(8.0),
                            onTap: () {},
                            child: Card(
                              child: Padding(
                                padding: EdgeInsets.all(15.0),
                                child: RaisedButton.icon(
                                  onPressed: null,
                                  icon: Icon(
                                    Icons.mic_none,
                                    color: Colors.white,
                                  ),
                                  label: Text(
                                    'record sound'.toUpperCase(),
                                    style: TextStyle(
                                        fontSize: 9, color: Colors.white),
                                  ),
                                  disabledColor: Colors.red,
                                ),
                              ),
                              color: HexColor("#dde9ff"),
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ],
          ),
        ));
  }
}
