import 'dart:math';
import 'package:hexcolor/hexcolor.dart';
import 'package:releep_app/utils/constants.dart';
import 'package:vector_math/vector_math_64.dart';
import 'package:flutter/material.dart';

class MenuShake extends StatefulWidget {
  @override
  _MenuShake createState() => _MenuShake();
}

class _MenuShake extends State<MenuShake> with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation;
  double shakeRate = 10;
  bool status = false;
  double _currentSliderValue = 10;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 5),
    )..addListener(() => setState(() {}));

    animation = Tween<double>(
      begin: 50.0,
      end: 60.0,
    ).animate(animationController);

    animationController.forward();
  }

  Vector3 _shake() {
    double progress = animationController.value;
    double offset = sin(progress * (30 * _currentSliderValue));
    return Vector3(offset * (status ? 20 : 0.0), 0.0, 0.0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Shake"),
        centerTitle: true,
        backgroundColor: HexColor("#633ce0"),
      ),
      body: Padding(
        padding: EdgeInsets.all(paddingContainer),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Transform(
              transform: Matrix4.translation(_shake()),
              child: Image.asset(
                'assets/images/bed.png',
                height: 280,
              ),
            ),
            Row(
              children: [
                Container(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "0",
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Slider(
                    value: _currentSliderValue,
                    min: 0,
                    max: 100,
                    divisions: 100,
                    activeColor: HexColor("#7044ff"),
                    inactiveColor: HexColor("#ddd3ff"),
                    label: _currentSliderValue.round().toString(),
                    onChanged: (double value) {
                      setState(() {
                        _currentSliderValue = value;
                      });
                    },
                  ),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  child: Text(
                    "100",
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Switch ON/OFF',
                  style: TextStyle(color: HexColor("#364bc4")),
                ),
                Switch(
                  value: status,
                  onChanged: (value) {
                    setState(() {
                      status = value;
                    });
                  },
                  activeTrackColor: HexColor("#7044ff"),
                  activeColor: HexColor("#c4b2ff"),
                ),
              ],
            ),
            Divider(
              color: HexColor("#000000"),
              height: 1,
            )
          ],
        ),
      ),
    );
  }
}
