import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:releep_app/utils/constants.dart';

class MenuSmell extends StatefulWidget {
  @override
  _MenuSmell createState() => _MenuSmell();
}

class _MenuSmell extends State<MenuSmell> {
  bool onTapCard1 = false;
  String colorCard1 = "#dde9ff";
  bool onTapCard2 = false;
  String colorCard2 = "#dde9ff";
  bool onTapCard3 = false;
  String colorCard3 = "#dde9ff";
  bool onTapCard4 = false;
  String colorCard4 = "#dde9ff";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Smell'),
          backgroundColor: HexColor("#633ce0"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(paddingContainer),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(
                height: 20,
              ),
              Image.asset(
                'assets/images/releep.png',
                height: 200,
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    borderRadius: BorderRadius.circular(8.0),
                    onTap: () {
                      setState(() {
                        onTapCard1 = !onTapCard1;
                        colorCard1 = onTapCard1 ? "#1ce370" : "#dde9ff";
                      });
                    },
                    child: Container(
                      width: 150,
                      height: 120,
                      child: Card(
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/smell.png',
                                height: 50,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                width: 80,
                                alignment: Alignment.center,
                                child: Text(
                                  'Lavander'.toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        color: HexColor(colorCard1),
                      ),
                    ),
                  ),
                  InkWell(
                    borderRadius: BorderRadius.circular(8.0),
                    onTap: () {
                      setState(() {
                        onTapCard2 = !onTapCard2;
                        colorCard2 = onTapCard2 ? "#1ce370" : "#dde9ff";
                      });
                    },
                    child: Container(
                      width: 150,
                      height: 120,
                      child: Card(
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/smell.png',
                                height: 50,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                width: 80,
                                alignment: Alignment.center,
                                child: Text(
                                  'coffee'.toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        color: HexColor(colorCard2),
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    borderRadius: BorderRadius.circular(8.0),
                    onTap: () {
                      setState(() {
                        onTapCard3 = !onTapCard3;
                        colorCard3 = onTapCard3 ? "#1ce370" : "#dde9ff";
                      });
                    },
                    child: Container(
                      width: 150,
                      height: 120,
                      child: Card(
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/smell.png',
                                height: 50,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                width: 80,
                                alignment: Alignment.center,
                                child: Text(
                                  'flower'.toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        color: HexColor(colorCard3),
                      ),
                    ),
                  ),
                  InkWell(
                    borderRadius: BorderRadius.circular(8.0),
                    onTap: () {
                      setState(() {
                        onTapCard4 = !onTapCard4;
                        colorCard4 = onTapCard4 ? "#1ce370" : "#dde9ff";
                      });
                    },
                    child: Container(
                      width: 150,
                      height: 120,
                      child: Card(
                        child: Padding(
                          padding: EdgeInsets.all(15.0),
                          child: Column(
                            children: [
                              Image.asset(
                                'assets/images/smell.png',
                                height: 50,
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                width: 80,
                                alignment: Alignment.center,
                                child: Text(
                                  'fish'.toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        color: HexColor(colorCard4),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }
}
