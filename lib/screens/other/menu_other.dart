import 'package:flutter/material.dart';
import 'package:country_picker/country_picker.dart';
import 'package:flutter_dropdown/flutter_dropdown.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:releep_app/utils/constants.dart';
import 'package:toast/toast.dart';
import 'package:pie_chart/pie_chart.dart';

class MenuOther extends StatefulWidget {
  @override
  _MenuOther createState() => _MenuOther();
}

class _MenuOther extends State<MenuOther> {
  String _yourCountry = "Slect Your Country";
  var _timeZone = 0;
  final List<String> entries = <String>[
    '30 Sep 2019 - Shake,Sound',
    '01 Oct 2019 - Sound',
    '02 Oct 2019 - Smell',
    '03 Oct 2019 - Shake,Sound'
  ];
  Map<String, double> dataMap = {
    "Sound": 5,
    "Smell": 3,
    "Shake": 2,
    "Sheet Roller": 2,
  };
  List<Color> colorList = [
    Colors.orange,
    Colors.lightBlueAccent,
    Colors.purple,
    Colors.lightGreen,
  ];

  int _counter = 0;
  List<Person> persons = [
    Person(
        gender: "Male",
        url: "https://images.unsplash.com/photo-1555952517-2e8e729e0b44"),
    Person(
        gender: "Female",
        url: "https://images.unsplash.com/photo-1555952517-2e8e729e0b44"),
    Person(
        gender: "Other",
        url: "https://images.unsplash.com/photo-1555952517-2e8e729e0b44"),
  ];

  Person selectedPerson;

  void _incrementCounter() {
    setState(() {
      _counter++;
      selectedPerson = null;
    });
  }

  @override
  void initState() {
    selectedPerson = persons.first;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Setting'),
        backgroundColor: HexColor("#633ce0"),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(paddingContainer),
        child: Column(
          children: [
            Container(
              width: double.infinity,
              child: Card(
                elevation: 5,
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'PROFILE',
                        style: TextStyle(
                            color: HexColor('#364bc4'),
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      new Theme(
                        data: new ThemeData(
                            primaryColor: Colors.grey[350],
                            accentColor: Colors.grey[350],
                            hintColor: Colors.grey[350]),
                        child: TextField(
                          keyboardType: TextInputType.name,
                          decoration: new InputDecoration(
                              hintText: "Enter your Name",
                              labelText: "Your Name",
                              labelStyle: new TextStyle(color: Colors.black87),
                              border: new UnderlineInputBorder(
                                  borderSide:
                                      new BorderSide(color: Colors.red))),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      InkWell(
                        onTap: () {
                          showCountryPicker(
                            context: context,
                            //Optional.  Can be used to exclude(remove) one ore more country from the countries list (optional).
                            exclude: <String>['KN', 'MF'],
                            //Optional. Shows phone code before the country name.
                            // showPhoneCode: true,
                            onSelect: (Country country) {
                              print('Select country: ${country.displayName}');
                              setState(() {
                                _yourCountry = country.name;
                              });
                            },
                          );
                        },
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(_yourCountry),
                                Icon(Icons.arrow_drop_down_outlined)
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Divider(
                              color: Colors.grey,
                              height: 2,
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              onPressed: () {},
                              color: HexColor("#7044ff"),
                              child: Text(
                                'SAVE PROFILE',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              height: 420,
              width: double.infinity,
              child: Card(
                elevation: 5,
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'ACTIVITIES RECORD',
                        style: TextStyle(
                            color: HexColor('#364bc4'),
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      PieChart(
                        dataMap: dataMap,
                        animationDuration: Duration(milliseconds: 800),
                        chartLegendSpacing: 32,
                        chartRadius: MediaQuery.of(context).size.width / 3.2,
                        colorList: colorList,
                        initialAngleInDegree: 0,
                        chartType: ChartType.ring,
                        ringStrokeWidth: 32,
                        legendOptions: LegendOptions(
                          showLegendsInRow: false,
                          legendPosition: LegendPosition.right,
                          showLegends: true,
                          legendShape: BoxShape.circle,
                          legendTextStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        chartValuesOptions: ChartValuesOptions(
                          showChartValueBackground: true,
                          showChartValues: true,
                          showChartValuesInPercentage: true,
                          showChartValuesOutside: false,
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Date & Alarm Choice',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: HexColor("#364bc4"),
                              ),
                            ),
                            Divider(),
                            Text('30 Sep 2019 - Shake,Sound'),
                            Divider(),
                            Text('01 Oct 2019 - Sound'),
                            Divider(),
                            Text('02 Oct 2019 - Smell'),
                            Divider(),
                            Text('03 Oct 2019 - Shake,Sound'),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              child: Card(
                elevation: 5,
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'TIME ZONE',
                        style: TextStyle(
                            color: HexColor('#364bc4'),
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: DropDown<String>(
                              items: <String>["Male", "Female", "Other"],
                              customWidgets: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Text("Male"),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Text("Female"),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Text("Other"),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      // InkWell(
                      //   onTap: () async {
                      //     // _asyncSimpleDialog(context);
                      //     // _asyncConfirmDialog(context);
                      //     // _asyncInputDialog(context);
                      //     final Departments deptName =
                      //         await _asyncSimpleDialog(context);
                      //     print("Selected Departement is $deptName");
                      //
                      //     setState(() {
                      //       _timeZone = deptName.toString();
                      //     });
                      //   },
                      //   child: Column(
                      //     children: [
                      //       Row(
                      //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //         children: [
                      //           Text(_timeZone),
                      //           Icon(Icons.arrow_drop_down_outlined)
                      //         ],
                      //       ),
                      //       SizedBox(
                      //         height: 5,
                      //       ),
                      //       Divider(
                      //         color: Colors.grey,
                      //         height: 2,
                      //       )
                      //     ],
                      //   ),
                      // ),

                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }
}

class Person {
  final String gender;
  final String name;
  final String url;

  Person({this.name, this.gender, this.url});
}
