import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:releep_app/models/alarm.dart';
import 'package:releep_app/services/firebase/firebase_alarm_service.dart';
import 'package:releep_app/services/pref/pref_alarm_service.dart';
import 'package:releep_app/utils/constants.dart';

import '../main.dart';

class AlarmProvider with ChangeNotifier {
  final firestoreInstance = FirebaseFirestore.instance;
  var isLoading = false;
  var isEmty = false;
  List<Alarm> alarms = [];

  Future<void> getAlarm() async {
    var alarmFormPref = await getAlarmFromPref(ALARM_KEY_PREF);
    alarms.clear();
    alarms = alarmFormPref;

    //เช็คว่าถ้าไม่มีรายการแจ้งเตือนจาก pref ให้ไปทำการลบรายการใน firebase ด้วย
    if (alarms.length == 0) {
      await deleteAllAlarmFirebase();
    } else {
      await insertAllAlarmToFirestore(alarms);
    }
    notifyListeners();
  }

  Future<void> addAlarm(Alarm alarm) async {
    alarms.insert(0, alarm);
    saveAlarmToPref(alarms);
    await insertAlarmToFirestore(alarm);
    notifyListeners();
  }

  Future<void> updateEnableAlarm(int index, bool isEnable) async {
    alarms[index].enableAlarm = isEnable;
    await saveAlarmToPref(alarms);
    await updateEnableAlarmFirestore(alarms, index, isEnable);
    notifyListeners();
  }

  Future<void> resetAlarmAll() async {
    alarms.forEach((element) {
      element.enableAlarm = false;
    });
    await saveAlarmToPref(alarms);
    await resetAlarmFirestore(alarms);
    notifyListeners();
  }

  Future<void> deleteAlarm(int index) async {
    var docID = alarms[index].id;
    alarms.removeAt(index);
    await saveAlarmToPref(alarms);
    await delAlarmFirestore(docID);
    notifyListeners();
  }

  // void scheduleAlarm({DateTime scheduledNotificationDateTime, Alarm alarmInfo}) async {
  //   var today = new DateTime.now();
  //   var testDateTime = today.add(new Duration(minutes: 1));
  //   print(testDateTime);
  //
  //   var androidPlatformChannelSpecifics = AndroidNotificationDetails(
  //     'alarm_notif',
  //     'alarm_notif',
  //     'Channel for Alarm notification',
  //     priority: Priority.high,
  //     importance: Importance.max,
  //     icon: 'releep',
  //     sound: RawResourceAndroidNotificationSound('a_long_cold_sting'),
  //     largeIcon: DrawableResourceAndroidBitmap('releep'),
  //   );
  //
  //   var iOSPlatformChannelSpecifics = IOSNotificationDetails(
  //       sound: 'a_long_cold_sting.wav',
  //       presentAlert: true,
  //       presentBadge: true,
  //       presentSound: true);
  //   var platformChannelSpecifics = NotificationDetails(
  //       android: androidPlatformChannelSpecifics,
  //       iOS: iOSPlatformChannelSpecifics);
  //   // var platformChannelSpecifics = NotificationDetails(
  //   //     androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
  //
  //   // var time = Time(23, 41, 0);
  //   // await flutterLocalNotificationsPlugin.showDailyAtTime(
  //   //   0,
  //   //   'Test Title at ',
  //   //   'Test Body', //null
  //   //   time,
  //   //   platformChannelSpecifics,
  //   //   payload: 'Test Payload',
  //   // );
  //
  //   await flutterLocalNotificationsPlugin.schedule(0, 'Office', "TEST",
  //       testDateTime, platformChannelSpecifics).timeout(Duration(minutes: 2));
  // }
}
