import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:country_picker/country_picker.dart';
import 'package:provider/provider.dart';
import 'package:releep_app/providers/alarm_provider.dart';
import 'package:releep_app/screens/band/menu_band.dart';
import 'package:releep_app/screens/home/menu_home.dart';
import 'package:releep_app/screens/other/menu_other.dart';
import 'package:releep_app/screens/shake/menu_shake.dart';
import 'package:releep_app/screens/smell/menu_smell.dart';
import 'package:releep_app/screens/sound/menu_sound.dart';
import 'package:releep_app/screens/time/menu_time.dart';

// final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
//     FlutterLocalNotificationsPlugin();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) {
          return AlarmProvider();
        })
      ],
      child: MaterialApp(
        supportedLocales: [
          const Locale('en'),
          const Locale('el'),
          const Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hans'),
          // Generic Simplified Chinese 'zh_Hans'
          const Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hant'),
          // Generic traditional Chinese 'zh_Hant'
        ],
        localizationsDelegates: [
          CountryLocalizations.delegate,
        ],
        home: MyNavigationBar(),
      ),
    );
  }
}

class MyNavigationBar extends StatefulWidget {
  MyNavigationBar({Key key}) : super(key: key);



  @override
  _MyNavigationBarState createState() => _MyNavigationBarState();
}

class _MyNavigationBarState extends State<MyNavigationBar> {
  int _selectedIndex = 0;
  List<Widget> _MenuList = <Widget>[
    MenuHome(),
    MenuSmell(),
    MenuSound(),
    MenuShake(),
    MenuBand(),
    MenuTime(),
    MenuOther()
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _MenuList[_selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.fixed,
        iconSize: 30,
        selectedFontSize: 18,
        elevation: 10,
        items: [
          BottomNavigationBarItem(
            icon: Image.asset(
              'assets/images/home.png',
              height: 20,
            ),
            title: Text(
              'Home',
              style: TextStyle(fontSize: 10, color: Colors.black54),
            ),
            backgroundColor: Colors.white,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              'assets/images/smell.png',
              height: 20,
            ),
            title: Text(
              'Smell',
              style: TextStyle(fontSize: 10, color: Colors.black54),
            ),
            backgroundColor: Colors.white,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              'assets/images/sound.png',
              height: 20,
            ),
            title: Text(
              'Sound',
              style: TextStyle(fontSize: 10, color: Colors.black54),
            ),
            backgroundColor: Colors.white,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              'assets/images/shake.png',
              height: 20,
            ),
            title: Text(
              'Shake',
              style: TextStyle(fontSize: 10, color: Colors.black54),
            ),
            backgroundColor: Colors.white,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              'assets/images/band.png',
              height: 20,
            ),
            title: Text(
              'Band',
              style: TextStyle(fontSize: 10, color: Colors.black54),
            ),
            backgroundColor: Colors.white,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              'assets/images/time.png',
              height: 20,
            ),
            title: Text(
              'Time',
              style: TextStyle(fontSize: 10, color: Colors.black54),
            ),
            backgroundColor: Colors.white,
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              'assets/images/other.png',
              height: 20,
            ),
            title: Text(
              'Other',
              style: TextStyle(fontSize: 10, color: Colors.black54),
            ),
            backgroundColor: Colors.white,
          ),
        ],
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
    );
  }
}
