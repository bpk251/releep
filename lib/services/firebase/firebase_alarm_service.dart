import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:releep_app/models/alarm.dart';

final firestoreInstance = FirebaseFirestore.instance;

Future<void> getAlarmFromFirebase() async {
  List<Alarm> alarms;
  await firestoreInstance.collection('Alarm').get().then((querySnapshot) {
    querySnapshot.docs.forEach((result) {
      print("result : ${result.data()}");
      var data = result.data();
      if (!data.isEmpty) {
        Alarm alarm = Alarm(
            id: result.id,
            date: data['date'],
            time: data['time'],
            smell: data['smell'],
            sound: data['sound'],
            enableShake: data['enableShake'],
            enableRoller: data['enableRoller'],
            enableAlarm: data['enableAlarm'],
            valueShake: data['valueShake']);
        alarms.add(alarm);
      }
      print('alarm : $alarms');
    });
  }).catchError((err) => print('err : ${err}'));
}

Future<void> deleteAllAlarmFirebase() async {
  await firestoreInstance
      .collection('Alarm')
      .get()
      .then((querySnapshot) => querySnapshot.docs.forEach((result) {
            delAlarmFirestore(result.id);
          }));
}

Future<void> insertAllAlarmToFirestore(List<Alarm> alarms) async {
  alarms.forEach((element) {
    Map<String, dynamic> _mapData = Map();
    _mapData['date'] = element.date;
    _mapData['time'] = element.time;
    _mapData['smell'] = element.smell;
    _mapData['sound'] = element.sound;
    _mapData['enableAlarm'] = element.enableAlarm;
    _mapData['enableShake'] = element.enableShake;
    _mapData['enableRoller'] = element.enableRoller;
    _mapData['valueShake'] = element.valueShake;

    firestoreInstance
        .collection('Alarm')
        .doc(element.id)
        .set(_mapData)
        .then((value) {
      print('insert to firebase success');
    }).catchError((err) => {print('error is : $err')});
  });
}

Future<void> insertAlarmToFirestore(Alarm alarm) async {
  Map<String, dynamic> _mapData = Map();
  _mapData['date'] = alarm.date;
  _mapData['time'] = alarm.time;
  _mapData['smell'] = alarm.smell;
  _mapData['sound'] = alarm.sound;
  _mapData['enableAlarm'] = alarm.enableAlarm;
  _mapData['enableShake'] = alarm.enableShake;
  _mapData['enableRoller'] = alarm.enableRoller;
  _mapData['valueShake'] = alarm.valueShake;

  firestoreInstance
      .collection('Alarm')
      .doc(alarm.id)
      .set(_mapData)
      .then((value) {
    print('insert to firebase success');
  }).catchError((err) => {print('error is : $err')});
}

Future<void> delAlarmFirestore(String docID) async {
  firestoreInstance.collection('Alarm').doc(docID).delete().then((value) {
    print('delete success');
  }).catchError((err) {
    print(err);
  });
}

Future<void> resetAlarmFirestore(List<Alarm> alarms) async {
  alarms.forEach((element) {
    element.enableAlarm = false;
    firestoreInstance
        .collection('Alarm')
        .doc(element.id)
        .update({'enableAlarm': false});
  });
}

Future<void> updateEnableAlarmFirestore(
    List<Alarm> alarms, int index, bool isEnable) async {
  firestoreInstance
      .collection('Alarm')
      .doc(alarms[index].id)
      .update({'enableAlarm': isEnable});
}
