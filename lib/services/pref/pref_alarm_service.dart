
import 'dart:convert';
import 'package:releep_app/models/alarm.dart';
import 'package:releep_app/utils/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

saveAlarmToPref(List<Alarm> alarms) async {
  var json = jsonEncode(alarms.map((e) => e.toJson()).toList()); //covert object to map
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString(ALARM_KEY_PREF, json);
}

Future<List<Alarm>> getAlarmFromPref(String key) async {
  final prefs = await SharedPreferences.getInstance();
  Iterable l = json.decode(prefs.getString(key));
  List<Alarm> alarms = List<Alarm>.from(l.map((model)=> Alarm.fromJson(model)));
  return alarms;
}