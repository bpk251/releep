// To parse this JSON data, do
//
//     final alarm = alarmFromJson(jsonString);

import 'dart:convert';

Alarm alarmFromJson(String str) => Alarm.fromJson(json.decode(str));

String alarmToJson(Alarm data) => json.encode(data.toJson());

class Alarm {
  Alarm({
    this.id,
    this.date,
    this.enableShake,
    this.enableAlarm,
    this.sound,
    this.valueShake,
    this.time,
    this.smell,
    this.enableRoller,
    this.dateString,
  });

  String id;
  List<String> date;
  bool enableShake;
  bool enableAlarm;
  String sound;
  double valueShake;
  String time;
  String smell;
  bool enableRoller;
  String dateString;

  factory Alarm.fromJson(Map<String, dynamic> json) => Alarm(
    id: json["id"],
    date: List<String>.from(json["date"].map((x) => x)),
    enableShake: json["enableShake"],
    enableAlarm: json["enableAlarm"],
    sound: json["sound"],
    valueShake: json["valueShake"],
    time: json["time"],
    smell: json["smell"],
    enableRoller: json["enableRoller"],
    dateString: json["dateString"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "date": List<dynamic>.from(date.map((x) => x)),
    "enableShake": enableShake,
    "enableAlarm": enableAlarm,
    "sound": sound,
    "valueShake": valueShake,
    "time": time,
    "smell": smell,
    "enableRoller": enableRoller,
    "dateString": dateString,
  };
}
